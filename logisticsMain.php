<?php
session_start();
require("logisticsModel.php");

if (!isset($_SESSION['loginProfile'])) {
	//* if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>This is the LOGISTICS Main page
		[<a href="logout.php">logout</a>]
	</p>
	<hr>
	<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
		", Your ID is: ",
		$_SESSION["loginProfile"]["uID"],
    ", Your Role is: ";
  if($_SESSION["loginProfile"]["uRole"] == 0)
    echo "Member<HR>";
  else if($_SESSION["loginProfile"]["uRole"] == 1)
    echo " Staff<HR>";
  else if($_SESSION["loginProfile"]["uRole"] == 2)
    echo "Logistics<hr>";

	$result = getNotShippedList();
	?>
  <br>
  <!-- <a href="orderStatus.php" target="_self">List Orders</a> -->
	<!-- <a href="productHandler.php?act=add" target="_self">Add a new product</a> -->
	<br>
	<table width="200" border="1">
		<tr>
			<td>ordID</td>
			<td>uID</td>
			<td>address</td>
      <td>actions</td>
      <!-- <td>Delete</td> -->
		</tr>
		<?php
		while ($rs = mysqli_fetch_assoc($result)) {
			echo "<tr><td>" . $rs['ordID'] . "</td>";
			echo "<td>{$rs['uID']}</td>";
			echo "<td>", $rs['address'], "</td>";
      echo "<td><a href='LogisticsControl.php?ordID=" . $rs['ordID'] . "' target='_self'>Modify</a></td>";
			// echo "<td><a href='productHandler.php?act=delete&prdID=" . $rs['prdID'] . "' target='_self'>+</a></td>";
			echo "</tr>";
		}
		?>
	</table>

<?php
// if(isset($_GET['modify'])){
// 	echo "<script>alert('Modify status as Shipped successfully!')</script>";
// }

?>


</body>

</html>