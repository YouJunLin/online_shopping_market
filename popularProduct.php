<?php
session_start();
require("popularProductModel.php");

if (!isset($_SESSION['loginProfile'])) {
	//* if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>This is the POPULAR PRODUCT page
		[<a href="logout.php">logout</a>]
	</p>
	<hr>
	<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
		", Your ID is: ",
		$_SESSION["loginProfile"]["uID"],
    ", Your Role is: ";
  if($_SESSION["loginProfile"]["uRole"] == 0)
    echo "Member<HR>";
  else
    echo " Staff<HR>";
	$result = getSellNum();
	?>
	<br>
  <!-- <a href="popularProduct.php" target="_self">List popular products</a> -->
  <a href="productManagement.php" target="_self">Back to productManagement</a><br>
  <a href="orderStatus.php" target="_self">List Orders</a>
	<a href="productHandler.php?act=add" target="_self">Add a new product</a>
  <br>
  <P>Best Sold Item</P>
	<table width="200" border="1">
		<tr>
			<td>prdID</td>
			<td>name</td>
			<td>number</td>
      <!-- <td>Manage</td> -->
      <!-- <td>Delete</td> -->
		</tr>
		<?php
		foreach ($result as $key => $row) {
      // while ($rs = mysqli_fetch_assoc($result)) {
      echo "<tr><td>" . $row['prdID'] . "</td>";
			echo "<td>{$row['name']}</td>";
			echo "<td>{$row['num']}</td>";
      // echo "<td>", $rs['num'], "</td>";
      // echo "<td><a href='productHandler.php?act=manage&prdID=" . $rs['prdID'] . "' target='_self'>Go Go</a></td>";
			// echo "<td><a href='productHandler.php?act=delete&prdID=" . $rs['prdID'] . "' target='_self'>+</a></td>";
			echo "</tr>";
		}
		?>
	</table>

	<?php
	if (isset($_GET['act'])) {
		if ($_GET['act'] == 'addToCart') { ?>
			<script>
				var res = confirm("Add to cart successfully!");
			</script>
	<?php
		}
	}
	?>


</body>

</html>