<?php
require_once("dbconfig.php");

function getSellNum()
{
  global $db;
  /* $sql = "SELECT product.prdID, name, a.num, detail 
  FROM product, (SELECT prdID, COUNT(quantity) num 
                 FROM orderitem) `a` 
  ORDER BY a.num"; */
  $result = getUserOrdID();
  $data = [];
  while ($rs = mysqli_fetch_assoc($result)) {
    // echo $rs['prdID'];
    $sql = 
    " SELECT prdID, SUM(price) amount 
      FROM orderitem
      WHERE 
        orderitem.ordID = ?";
    $stmt = mysqli_prepare($db, $sql);
    mysqli_stmt_bind_param($stmt, 'i', $rs['ordID']);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    if ($rr = mysqli_fetch_assoc($res)) {
      // print_r($rr);
      array_push($data, array('uID' => $rs['prdID'], 'name' => $rs['name'], 'amount' => $rr['amount']));
    }

    // Obtain a list of columns
    foreach ($data as $key => $row) {
      $prdID[$key]  = $row['prdID'];
      $name[$key] = $row['name'];
      $amount[$key] = $row['amount'];
    }

    $prdID  = array_column($data, 'prdID');
    $name = array_column($data, 'name');
    $amount = array_column($data, 'amount');

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($amount, SORT_DESC, $prdID, SORT_ASC, $name, SORT_ASC, $data);
  }
  // echo "<br>";
  // print_r($data);
  return $data;
}

function getUserUID($role = 0)
{
  global $db;
  $sql = "SELECT uID, name FROM product WHERE role = ?";
  $stmt = mysqli_prepare($db, $sql); //prepare sql statement
  mysqli_stmt_bind_param($stmt, "i", $role); //bind parameters with variables
  mysqli_stmt_execute($stmt);  //執行SQL
  $result = mysqli_stmt_get_result($stmt); //get the results
  return $result;
}

function getUserOrdID()
{
  $result = getUserUID();

  global $db;
  $data = [];
  while ($rs = mysqli_fetch_assoc($result)) {
    $sql = "SELECT ordID, uID FROM product WHERE uID = ? status > 0";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $rs['uID']); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $res = mysqli_stmt_get_result($stmt); //get the results
    while ($rr = mysqli_fetch_assoc($res)) {
      array_push($data, array('ordID' => $rr['ordID'], 'uID' => $rs['uID']));
    }
  }
  return $data;
}
