<?php
session_start();
require("productModel.php");

if (!isset($_SESSION['loginProfile'])) {
	//* if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>This is the PRODUCT MANAGEMENT page
		[<a href="logout.php">logout</a>]
	</p>
	<hr>
	<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
		", Your ID is: ",
		$_SESSION["loginProfile"]["uID"],
    ", Your Role is: ";
  if($_SESSION["loginProfile"]["uRole"] == 0)
    echo "Member<HR>";
  else
    echo " Staff<HR>";
	$result = getProductList();
	?>
	<br>
	<a href="popularProduct.php" target="_self">List popular products</a>
  <a href="orderStatus.php" target="_self">List Orders</a>
	<a href="productHandler.php?act=add" target="_self">Add a new product</a>
	<a href="ViewVIP.php" target="_self">View VIP</a>
	<br>
	<table width="200" border="1">
		<tr>
			<td>id</td>
			<td>name</td>
			<td>price</td>
      <td>Manage</td>
      <!-- <td>Delete</td> -->
		</tr>
		<?php
		while ($rs = mysqli_fetch_assoc($result)) {
			echo "<tr><td>" . $rs['prdID'] . "</td>";
			echo "<td>{$rs['name']}</td>";
			echo "<td>", $rs['price'], "</td>";
      echo "<td><a href='productHandler.php?act=manage&prdID=" . $rs['prdID'] . "' target='_self'>Go Go</a></td>";
			// echo "<td><a href='productHandler.php?act=delete&prdID=" . $rs['prdID'] . "' target='_self'>+</a></td>";
			echo "</tr>";
		}
		?>
	</table>

	<?php
	if (isset($_GET['act'])) {
		if ($_GET['act'] == 'addToCart') { ?>
			<script>
				var res = confirm("Add to cart successfully!");
			</script>
	<?php
		}
	}
	?>


</body>

</html>