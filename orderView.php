<?php
session_start();
require("orderModel.php");

if (!isset($_SESSION['loginProfile'])) {
	// if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>This is the ORDER page
		[<a href="logout.php">logout</a>]
	</p>
	<hr>
	<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
		", Your ID is: ",
		$_SESSION["loginProfile"]["uID"],
		", Your Role is: ";
	if ($_SESSION["loginProfile"]["uRole"] == 0)
		echo " Member<HR>";
	else
		echo " Staff<HR>";

	$result = getOrderList($_SESSION["loginProfile"]['uID']);
	?>
	<table width="850" border="1">
		<tr>
			<td>order ID</td>
			<td>Date</td>
			<td>status</td>
			<td>address</td>
			<td>detail</td>
		</tr>
		<?php
		$SA = ["待寄出", "已寄出", "已送達"];
		while ($rs = mysqli_fetch_assoc($result)) {
			echo "<tr><td>" . $rs['ordID'] . "</td>";
			echo "<td>{$rs['orderDate']}</td>";
			echo "<td>", $SA[$rs['status']-1], "</td>";
			echo "<td>{$rs['address']}</td>";
			$tmp = "'block'"; ?>
			<td><button onclick="
						var ele = document.querySelector('#orderDetail-<?php echo $rs['ordID']; ?>');
						ele.style.display='block'">more info
				</button>
			</td>
			<!-- //* more info panel -->
			<?php $detail = getOrderDetail($rs['ordID']); ?>
			<td>
				<div id='orderDetail-<?php echo $rs['ordID']; ?>' class='w3-panel w3-display-container' style="margin: 0px; padding: 10px; display: none">
					<span onclick="this.parentElement.style.display='none'" class='w3-button w3-large w3-gray w3-display-topright' style="margin: 0px; padding:0px">close</span>
					<table border='1'>
						<tr>
							<td>product ID</td>
							<td>product ID</td>
							<td>quantity</td>
							<td>price</td>
						</tr>
					<?php
						while ($ds = mysqli_fetch_assoc($detail)) {
							echo "<tr><td>" . $ds['prdID'] . "</td>";
							echo "<td>" . $ds['name'] . "</td>";
							echo "<td>", $ds['quantity'], "</td>";
							echo "<td>", $ds['price'], "</td>";
							echo "</tr>";
						}
						echo "</table>";
						echo "</div></td>";
						echo "</tr>";
					}
					?>
					</table>
					<a href=" mainUI.php" target="_self">Back to main</a>


</body>

</html>