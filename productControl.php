<?php
require("productModel.php");
/* if (!isset($_SESSION['loginProfile'])) {
	// if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
} */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Basic HTML Examples</title>
</head>

<body>
  <p>This is a PRODUCT Control page</p>
  <hr>

  <?php

  if (isset($_GET['act'])) {
    $act = $_GET['act'];
    if ($act != "add")
      $prdID = $_GET['prdID'];
  }

  $name = $_POST['name'];
  $price = $_POST['price'];
  $detail = $_POST['detail'];

  //* remove the product
  if ($act == 'remove') {
    $result = removeProduct($prdID);
  } elseif ($act == 'modify') {
    $result = modifyProduct($prdID, $name, $price, $detail);
  } elseif ($act == 'add') {
    $result = addProduct($name, $price, $detail);
  } else
    echo "ERROR EXCEPTION: invalid agument 'act'!<HR>";

  if ($result == true)
    // echo "success<hr>";
    header("Location: productManagement.php");
  else
    echo "ERROR EXCEPTION: failed to '{$act}'! ";
  ?>

</body>

</html>