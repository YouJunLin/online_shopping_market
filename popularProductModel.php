<?php
require_once("dbconfig.php");

function getSellNum()
{
  global $db;
  /* $sql = "SELECT product.prdID, name, a.num, detail 
  FROM product, (SELECT prdID, COUNT(quantity) num 
                 FROM orderitem) `a` 
  ORDER BY a.num"; */
  $result = getAllPIDAndName();
  $data = [];
  while ($rs = mysqli_fetch_assoc($result)) {
    // echo $rs['prdID'];
    $sql = 
    " SELECT prdID, SUM(quantity) num 
      FROM orderitem
      WHERE 
        orderitem.prdID = ?";
    $stmt = mysqli_prepare($db, $sql);
    mysqli_stmt_bind_param($stmt, 'i', $rs['prdID']);
    mysqli_stmt_execute($stmt);
    $res = mysqli_stmt_get_result($stmt);
    if ($rr = mysqli_fetch_assoc($res)) {
      // print_r($rr);
      array_push($data, array('prdID' => $rr['prdID'], 'name' => $rs['name'], 'num' => $rr['num']));
    }

    // Obtain a list of columns
    foreach ($data as $key => $row) {
      $prdID[$key]  = $row['prdID'];
      $name[$key] = $row['name'];
      $num[$key] = $row['num'];
    }

    $prdID  = array_column($data, 'prdID');
    $name = array_column($data, 'name');
    $num = array_column($data, 'num');

    // Sort the data with volume descending, edition ascending
    // Add $data as the last parameter, to sort by the common key
    array_multisort($num, SORT_DESC, $prdID, SORT_ASC, $name, SORT_ASC, $data);
  }
  // echo "<br>";
  // print_r($data);
  return $data;
}

function getAllPIDAndName()
{
  global $db;
  $sql = "SELECT prdID, name FROM product";
  $stmt = mysqli_prepare($db, $sql); //prepare sql statement
  // mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
  mysqli_stmt_execute($stmt);  //執行SQL
  $result = mysqli_stmt_get_result($stmt); //get the results
  return $result;
}
