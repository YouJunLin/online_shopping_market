<?php
  require_once("dbconfig.php");
	
	function getNotShippedList()
  {
	  global $db;
	  $sql = "SELECT ordID, uID, address FROM userOrder WHERE status = 1 ORDER BY address";
	  $stmt = mysqli_prepare($db, $sql); //prepare sql statement
	  mysqli_stmt_execute($stmt);  //執行SQL
	  $result = mysqli_stmt_get_result($stmt); //get the results
	  return $result;
	}
	
	function modifyStatusAsShipped($status, $ordID){
		global $db;
		$sql = "UPDATE userorder SET status=? WHERE ordID=? ";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "ii", $status, $ordID); //bind parameters with variables
		$result = mysqli_stmt_execute($stmt);  //執行SQL
		// $result = mysqli_stmt_get_result($stmt); //get the results
		return $result;
	}
  
?>